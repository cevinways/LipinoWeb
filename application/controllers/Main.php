<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('M_data');
    }
    public function index(){
        if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
			if($this->M_data->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
				$this->M_data->save(); // Panggil fungsi save() yang ada di SiswaModel.php
				redirect('main/akun');
			}
		}
        $this->load->view('view_page_header');
        $this->load->view('view_page_main');
        $this->load->view('view_page_footer');
    }
    public function akun(){
        $data['user'] = $this->M_data->view();
        $this->load->view('view_page_header');
        $this->load->view('view_page_akun', $data);
        $this->load->view('view_page_footer');
    }
    public function ubah($nik){
        if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
			if($this->M_data->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
				$this->M_data->edit($nik); // Panggil fungsi edit() yang ada di M_dat
				redirect('main/akun');
			}
		}
		
        $data['user'] = $this->M_data->view_by($nik);
        $this->load->view('view_page_header');
        $this->load->view('view_page_edit', $data);
        $this->load->view('view_page_footer');
    }
    public function hapus($nik){
		$this->M_data->delete($nik); // Panggil fungsi delete() yang ada di SiswaModel.php
		redirect('main/akun');
    }
    public function tentang(){
        $this->load->view('view_page_header');
        $this->load->view('view_page_tentang');
        $this->load->view('view_page_footer');
    }
    public function bantuan(){
        $this->load->view('view_page_header');
        $this->load->view('view_page_bantuan');
        $this->load->view('view_page_footer');
    }
    public function pengaturan(){
        $this->load->view('view_page_header');
        $this->load->view('view_page_pengaturan');
        $this->load->view('view_page_footer');
    }
}

?>