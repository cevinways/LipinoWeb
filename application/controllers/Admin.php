<?php 

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}

	function index(){
		$this->load->view('view_page_header');
		$this->load->view('view_page_main');
		$this->load->view('view_page_footer');
	}
}