<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data extends CI_Model{
	public function view(){
        return $this->db->get('user')->result();
	}
	// Fungsi untuk menampilkan data siswa berdasarkan NIS nya
	public function view_by($nik){
		$this->db->where('nik',$nik);
		return $this->db->get('user')->row();
	}
	// Fungsi untuk validasi form tambah dan ubah
	public function validation($mode){
		$this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
		
		// Tambahkan if apakah $mode save atau update
		// Karena ketika update, NIS tidak harus divalidasi
		// Jadi NIS di validasi hanya ketika menambah data siswa saja
		if($mode == "save")
			$this->form_validation->set_rules('input_nik', 'NIK', 'required|numeric|max_length[30]');
		
		$this->form_validation->set_rules('input_nama', 'Nama', 'required|max_length[50]');
		$this->form_validation->set_rules('input_ttl', 'Ttl', 'required|max_length[50]');
		$this->form_validation->set_rules('input_jk', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('input_alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('input_agama', 'Agama', 'required|max_length[50]');
		$this->form_validation->set_rules('input_status', 'Status', 'required|max_length[50]');
		$this->form_validation->set_rules('input_pekerjaan', 'Pekerjaan', 'required|max_length[50]');
		$this->form_validation->set_rules('input_kn', 'Kewarganegaraan', 'required|max_length[50]');
		$this->form_validation->set_rules('input_goldarah', 'Golongan Darah', 'required|max_length[50]');
			
		if($this->form_validation->run()) // Jika validasi benar
			return TRUE; // Maka kembalikan hasilnya dengan TRUE
		else // Jika ada data yang tidak sesuai validasi
			return FALSE; // Maka kembalikan hasilnya dengan FALSE
	}
	// Fungsi untuk melakukan simpan data ke tabel siswa
	public function save(){
		$data = array(
			"nik" => $this->input->post('input_nik'),
			"nama" => $this->input->post('input_nama'),
			"ttl" => $this->input->post('input_ttl'),
			"jk" => $this->input->post('input_jk'),
			"alamat" => $this->input->post('input_alamat'),
			"agama" => $this->input->post('input_agama'),
			"status" => $this->input->post('input_status'),
			"pekerjaan" => $this->input->post('input_pekerjaan'),
			"kn" => $this->input->post('input_kn'),
			"goldarah" => $this->input->post('input_goldarah')
		);
		
		$this->db->insert('user', $data); // Untuk mengeksekusi perintah insert data
	}
	// Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
	public function edit($nik){
		$data = array(
			"nama" => $this->input->post('input_nama'),
			"ttl" => $this->input->post('input_ttl'),
			"jk" => $this->input->post('input_jk'),
			"alamat" => $this->input->post('input_alamat'),
			"agama" => $this->input->post('input_agama'),
			"status" => $this->input->post('input_status'),
			"pekerjaan" => $this->input->post('input_pekerjaan'),
			"kn" => $this->input->post('input_kn'),
			"goldarah" => $this->input->post('input_goldarah')
		);
		
		$this->db->where('nik', $nik);
		$this->db->update('user', $data); // Untuk mengeksekusi perintah update data
	}
	// Fungsi untuk melakukan menghapus data siswa berdasarkan NIS siswa
	public function delete($nik){
		$this->db->where('nik', $nik);
		$this->db->delete('user'); // Untuk mengeksekusi perintah delete data
	}
}