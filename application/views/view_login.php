<!DOCTYPE html>
<html>
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/login_style.css">
    <body>
        <div class="bgimg w3-container w3-animate-opacity">
            <div class="w3-display-middle w3-text-white">
                <div class="stext w3-animate-top" style="text-align: center">
                    <h2 class="w3-jumbo">LIPINO</h2>
                    <h3>Kemudahan untuk Anda</h3>
                </div>
                <div class="w3-bar w3-center">
                    <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-brown w3-large">Masuk</button>
                    <button onclick="document.getElementById('id02').style.display='block'" class="w3-button w3-brown w3-large">Daftar</button>
                </div>
            </div>
            <!-- Sign In -->
            <div id="id01" class="w3-modal">
                <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
                    <div class="w3-center">
                        <br>
                        <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-transparent w3-display-topright"
                            title="Close Modal">x</span>
                    </div>
                    <form class="w3-container" action="<?php echo base_url('login/aksi_login')?>" method="post">
                        <div class="w3-section">
                            <label>
                                <b>Username</b>
                            </label>
                            <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Masukkan NIK" name="username" required>
                            <label>
                                <b>Password</b>
                            </label>
                            <input class="w3-input w3-border" type="password" placeholder="Enter Password" name="password" required>
                            <button class="w3-button w3-block w3-brown w3-section w3-padding" type="submit">Masuk</button>
                        </div>
                    </form>
                    <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                        <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-button w3-red">Batal</button>
                    </div>
                </div>
            </div>
            <!-- Sign Up -->
            <div id="id02" class="w3-modal">
                <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
                    <div class="w3-center">
                        <br>
                        <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-transparent w3-display-topright"
                            title="Close Modal">x</span>
                    </div>
                    <form class="w3-container" action="<?php echo base_url('login/register')?>" method="post">
                        <div class="w3-section">
                            <label>
                                <b>Username</b>
                            </label>
                            <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Masukkan Username" name="username" required>
                            <label>
                                <b>Email</b>
                            </label>
                            <input class="w3-input w3-border w3-margin-bottom" type="email" placeholder="@example.com" name="email" required>
                            <label>
                                <b>New Password</b>
                            </label>
                            <input class="w3-input w3-border" type="password" name="password" required>
                            <button class="w3-button w3-block w3-brown w3-section w3-padding" type="selesai">Submit</button>
                        </div>
                    </form>
                    <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                        <button onclick="document.getElementById('id02').style.display='none'" type="button" class="w3-button w3-red">Batal</button>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>