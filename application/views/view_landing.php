<!DOCTYPE html>
<html>
<title>Landing</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css">
<body>
    <!-- =================================INI BAGIAN HEADER ======================================-->
    <header>
        <!-- Navbar (sit on top) -->
        <div class="w3-top">
            <div class="w3-bar w3-white w3-card w3-text-brown" id="myNavbar">
                <a href="#home" class="w3-bar-item w3-button w3-wide">LIPINO</a>
                <!-- Right-sided navbar links -->
                <div class="w3-right w3-hide-small">
                    <a href="#about" class="w3-bar-item w3-button">
                        <i class="fa fa-question-circle"></i> TENTANG</a>
                    <a href="#contact" class="w3-bar-item w3-button">
                        <i class="fa fa-envelope"></i> CONTACT</a>
                </div>
                <!-- Hide right-floated links on small screens and replace them with a menu icon -->
                <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        <!-- Sidebar on small screens when clicking the menu icon -->
        <nav class="w3-sidebar w3-bar-block w3-brown w3-card w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close ×</a>
            <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button">TENTANG</a>
            <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button">CONTACT</a>
        </nav>
    </header>
    <!-- =================================INI BAGIAN INDEX (SECTION)======================================-->
    <!-- full-height image -->
    <div class="bgimg-1 w3-container w3-animate-opacity" id="home">
        <div class="w3-display-left w3-text-white" style="padding:48px">
            <span class="w3-jumbo w3-hide-small">Kemudahan Untuk Anda</span>
            <br>
            <span class="w3-xxlarge w3-hide-large w3-hide-medium">Kemudahan Untuk Anda</span>
            <br>
            <span class="w3-large">Membuat KTP jadi lebih mudah</span>
            <p>
                <a href="<?php echo base_url('index.php/login/')?>" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Daftar ></a>
            </p>
        </div>
    </div>
    <!-- About Section -->
    <div class="w3-container w3-text-brown" style="padding:128px 16px" id="about">
        <h3 class="w3-center">TENTANG LIPINO</h3>
        <div class="w3-row-padding w3-center" style="margin-top:64px">
            <div class="w3-quarter">
                <i class="fa fa-user w3-margin-bottom w3-jumbo w3-center"></i>
                <p class="w3-large">Kemudahan</p>
                <p>Mempermudah pekerja atau siswa sekolah yang umurnya mencukupi untuk membuat 
                KTP serta Meminimalisir penggunaan kertas dalam pengisian formulir</p>
            </div>
            <div class="w3-quarter">
                <i class="fa fa-heart w3-margin-bottom w3-jumbo"></i>
                <p class="w3-large">Keutamaan</p>
                <p>Masyarakat dapat dengan mudah membuat KTP di Kantor Desa, 
                Mempersingkat waktu pembuatan KTP, Tahapan alur 
                pembuatan lebih sederhana dan cepat</p>
            </div>
            <div class="w3-quarter">
                <i class="fa fa-map-marker w3-margin-bottom w3-jumbo"></i>
                <p class="w3-large">Target</p>
                <p>Target LIPINO adalah semua warga masyarakat dengan tujuan untuk 
                mempermudah para penduduk atau masyarakat untuk membuat Kartu Kependudukan.</p>
            </div>
            <div class="w3-quarter">
                <i class="fa fa-desktop w3-margin-bottom w3-jumbo"></i>
                <p class="w3-large">Akses</p>
                <p>Akses website ini dapa dilakukan kapanpun dan dimanapun 
                dengan membuka alamat website LIPINO yaitu lipino.ml.</p>
                </p>
            </div>
        </div>
    </div>
    <div class="w3-container w3-light-grey w3-text-brown" style="padding:128px 16px">
        <div class="w3-row-padding">
            <div class="w3-col m6">
                <h3>Jaminan Kemudahan.</h3>
                <p>Membantu masyarakat dalam pembuatan Kartu Tanda Penduduk
                    <br>Mempercepat proses pelayanan pembuatan</p>
                <p>
                    <a href="#work" class="w3-button w3-brown">
                        <i class="fa fa-th"> </i> Selanjutnya</a>
                </p>
            </div>
            <div class="w3-col m6">
                <img class="w3-image w3-round-large" src="<?php echo base_url("/assets/images/image3.jpg ");?>" alt="Pembuatan KTP" width="700"
                    height="394">
            </div>
        </div>
    </div>
    <div class="w3-container w3-text-brown" style="padding:128px 16px" id="work">
        <h3 class="w3-center">PEMBUATAN KTP OFFLINE</h3>
        <p class="w3-center w3-large">Pembuatan KTP secara offline</p>
        <div class="w3-row-padding" style="margin-top:64px">
            <div class="w3-col l3 m6">
                <img src="<?php echo base_url("/assets/images/1.jpg ");?>" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity"
                    alt="Pembuatan KTP">
            </div>
            <div class="w3-col l3 m6">
                <img src="<?php echo base_url("/assets/images/4.jpg ");?>" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity"
                    alt="Pembuatan KTP">
            </div>
            <div class="w3-col l3 m6">
                <img src="<?php echo base_url("/assets/images/3.jpg ");?>" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity"
                    alt="Pembuatan KTP">
            </div>
            <div class="w3-col l3 m6">
                <img src="<?php echo base_url("/assets/images/6.jpg ");?>" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity"
                    alt="Pembuatan KTP">
            </div>
        </div>
    </div>
    <!-- Modal for full size images on click-->
    <div id="modal01" class="w3-modal w3-black" onclick="this.style.display='none'">
        <span class="w3-button w3-xxlarge w3-black w3-padding-large w3-display-topright" title="Close Modal Image">×</span>
        <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
            <img id="img01" class="w3-image">
            <p id="caption" class="w3-opacity w3-large"></p>
        </div>
    </div>
    <!-- Team Section -->
    <div class="w3-container w3-text-brown w3-light-grey" style="padding:128px 16px" id="team">
        <h3 class="w3-center">TEAM PENGEMBANG</h3>
        <p class="w3-center w3-large">Dibalik LIPINO</p>
        <div class="w3-row-padding" style="margin-top:64px">
            <div class="w3-col l3 m6 w3-margin-bottom w3-grayscale-max">
                <div class="w3-card">
                    <img src="<?php echo base_url("/assets/images/cevin.jpeg ");?>" alt="Cevin" style="width:100%">
                    <div class="w3-container">
                        <h3>Cevin Ways</h3>
                        <p class="w3-opacity">Full Stack Developer</p>
                    </div>
                </div>
            </div>
            <div class="w3-col l3 m6 w3-margin-bottom w3-grayscale-max">
                <div class="w3-card">
                    <img src="<?php echo base_url("/assets/images/bekti.jpeg ");?>" alt="Bekti" style="width:100%">
                    <div class="w3-container">
                        <h3>Bekti Pitriani</h3>
                        <p class="w3-opacity">UI Designer</p>
                    </div>
                </div>
            </div>
            <div class="w3-col l3 m6 w3-margin-bottom w3-grayscale-max">
                <div class="w3-card">
                    <img src="<?php echo base_url("/assets/images/hardii.jpeg ");?>" alt="Ilham" style="width:100%">
                    <div class="w3-container">
                        <h3>Hardi Sartanto</h3>
                        <p class="w3-opacity">UX Designer</p>
                    </div>
                </div>
            </div>
            <div class="w3-col l3 m6 w3-margin-bottom w3-grayscale-max">
                <div class="w3-card">
                    <img src="<?php echo base_url("/assets/images/ilham.jpeg ");?>" alt="Hardi" style="width:100%">
                    <div class="w3-container">
                        <h3>Ilham Akbar</h3>
                        <p class="w3-opacity">UI Designer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact Section -->
    <div class="w3-container w3-text-brown" style="padding:128px 16px" id="contact">
        <h3 class="w3-center">CONTACT</h3>
        <p class="w3-center w3-large">Hubungi kami jika ada kendala di:</p>
        <div class="w3-row-padding" style="margin-top:64px">
            <div class="w3-half">
                <p>
                    <i class="fa fa-map-marker fa-fw w3-xxlarge w3-margin-right"></i> Malang, Jawa Timur</p>
                <p>
                    <i class="fa fa-phone fa-fw w3-xxlarge w3-margin-right"></i> Phone: +0341 585772</p>
                <p>
                    <i class="fa fa-envelope fa-fw w3-xxlarge w3-margin-right"> </i> Email: hellolipino.ml</p>
            </div>
            <div class="w3-half">
                <!-- Add Google Maps -->
                <div id="googleMap" style="width:100%;height:510px;"></div>
            </div>
        </div>
    </div>
    <!-- =================================INI BAGIAN FOOTER ======================================-->
    <footer class="w3-center w3-brown w3-padding-16">
        <a href="#home" class="w3-button w3-light-grey w3-text-brown">
            <i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
        <p>Copyright@lipino.ml</p>
    </footer>
    <!-- Add Google Maps -->
    <script>
        function myMap() {
            myCenter = new google.maps.LatLng(-7.9613831, 112.6152631);
            var mapOptions = {
                center: myCenter,
                zoom: 15, scrollwheel: false, draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
            var marker = new google.maps.Marker({
                position: myCenter,
            });
            marker.setMap(map);
        }
        // Modal Image Gallery
        function onClick(element) {
            document.getElementById("img01").src = element.src;
            document.getElementById("modal01").style.display = "block";
            var captionText = document.getElementById("caption");
            captionText.innerHTML = element.alt;
        }
        // Toggle between showing and hiding the sidebar when clicking the menu icon
        var mySidebar = document.getElementById("mySidebar");
        function w3_open() {
            if (mySidebar.style.display === 'block') {
                mySidebar.style.display = 'none';
            } else {
                mySidebar.style.display = 'block';
            }
        }
        // Close the sidebar with the close button
        function w3_close() {
            mySidebar.style.display = "none";
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_XpOa1G9iPJu3HAFfLtMyIAw3FKw5a3c&callback=myMap"></script>
        <!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->
</body>
</html>