<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:300px; margin-right: 150px">
    <div class="w3-row w3-padding-64">
        <div class="w3-card-4">
            <div class="w3-container w3-brown">
                <h2>Data Form Saya</h2>
            </div>
            <?php ?>
            <table class="w3-table-all">
            <?php
			if( ! empty($user)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
				foreach($user as $data){
                    echo 
                    "<tr>
                        <td>NIK : </td>
                        <td>".$data->nik."</td>
                    </tr>
                    <tr>
                        <td>Nama :</td>
                        <td>".$data->nama."</td>
                    </tr>
                    <tr>
                        <td>TTL : </td>
                        <td>".$data->ttl."</td>
                    </tr>
                    <tr>
                        <td>JK : </td>
                        <td>".$data->jk."</td>
                    </tr>
                    <tr>
                        <td>Alamat : </td>
                        <td>".$data->alamat."</td>
                    </tr>
                    <tr>
                        <td>Agama: </td>
                        <td>".$data->agama."</td>
                    </tr>
                    <tr>
                        <td>Status: </td>
                        <td>".$data->status."</td>
                    </tr>
                    <tr>
                        <td>Pekerjaan: </td>
                        <td>".$data->pekerjaan."</td>
                    </tr>
                    <tr>
                        <td>Kewarganegaraan: </td>
                        <td>".$data->kn."</td>
                    </tr>
                    <tr>
                        <td>Golongan Darah: </td>
                        <td>".$data->goldarah."</td>
                    </tr>
                    <tr>
                        <td>
                            <a href='".base_url("main/ubah/".$data->nik)."'>Ubah</a>
                            <a href='".base_url("main/hapus/".$data->nik)."'>Hapus</a>
                        </td>
                    </tr>";
				}
			}else{ // Jika data siswa kosong
				echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
			}
			?>
            </table>
        </div>
    </div>
</div>