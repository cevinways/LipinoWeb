<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:300px; margin-right: 150px">
    <div class="w3-row w3-padding-64">
        <div class="w3-card-4">
            <div class="w3-container w3-brown">
                <h2>Layanan Registrasi</h2>
            </div>
		    <?php echo form_open("main/index/"); ?>
            <table class="w3-table-all">
                <tr>
                    <td>NIK : </td>
                    <td><input type="text" name="input_nik" value="<?php echo set_value('input_nik'); ?>"></td>
                </tr>
                <tr>
                    <td>Nama: </td>
                    <td><input type="text" name="input_nama" value="<?php echo set_value('input_nama'); ?>"></td>
                </tr>
                <tr>
                    <td>TTL: </td>
                    <td><input type="text" name="input_ttl" value="<?php echo set_value('input_ttl'); ?>"></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin: </td>
                    <td><input type="radio" name="input_jk" value="Laki-laki" <?php echo set_radio('jk', 'Laki-laki'); ?>> Laki-laki</td>
                    <td><input type="radio" name="input_jk" value="Perempuan" <?php echo set_radio('jk', 'Perempuan'); ?>> Perempuan</td>
                </tr>
                <tr>
                    <td>Alamat: </td>
                    <td><textarea name="input_alamat"><?php echo set_value('input_alamat'); ?></textarea></td>
                </tr>
                <tr>
                    <td>Agama: </td>
                    <td><input type="text" name="input_agama" value="<?php echo set_value('input_agama'); ?>"></td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td><input type="text" name="input_status" value="<?php echo set_value('input_status'); ?>"></td>
                </tr>
                <tr>
                    <td>Pekerjaan: </td>
                    <td><input type="text" name="input_pekerjaan" value="<?php echo set_value('input_pekerjaan'); ?>"></td>
                </tr>
                <tr>
                    <td>Kewarganegaraan: </td>
                    <td><input type="text" name="input_kn" value="<?php echo set_value('input_kn'); ?>"></td>
                </tr>
                <tr>
                    <td>Golongan Darah: </td>
                    <td><input type="text" name="input_goldarah" value="<?php echo set_value('input_goldarah'); ?>"></td>
                </tr>
                <tr>
                    <td><input class="w3-button w3-brown" type="submit" name="submit" value="Simpan"></td>
                </tr>
        </table>
        <div style="background-color: red;"><?php echo validation_errors(); ?></div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>