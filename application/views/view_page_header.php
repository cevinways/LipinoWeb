<!DOCTYPE html>
<html>
<title>Main Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/page_style.css">

<body>
    <!-- Navbar -->
    <div class="w3-top">
        <div class="w3-bar w3-brown w3-top w3-left-align w3-large w3-animate-top">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large" href="javascript:void(0)" onclick="w3_open()">
                <i class="fa fa-bars"></i>
                <a href="#" class="w3-bar-item w3-brown w3-button">LIPINO</a>
        </div>
    </div>

    <!-- Sidebar -->
    <nav class="w3-sidebar w3-light-gray w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large"
            title="Close Menu">
            <i class="fa fa-remove"></i>
        </a>
        <div class="w3-container w3-row w3-padding-32">
            <div class="w3-col s4">
                <img src="<?php echo base_url("/assets/images/avatar1.png ");?>" class="w3-circle w3-margin-right" style="width:46px">
            </div>
            <div class="w3-col s8 w3-bar">
                <span>Selamat Datang</span>
                <br>
            </div>
        </div>
        <div class="w3-padding-32">
            <a class="w3-bar-item w3-button w3-hover-brown" href="<?php echo base_url('index.php/main/')?>">
                <i class="fa fa-home"></i> Beranda</a>
            <a class="w3-bar-item w3-button w3-hover-brown" href="<?php echo base_url('index.php/main/akun')?>">
                <i class="fa fa-file-text"></i> Form Saya</a>
            <a class="w3-bar-item w3-button w3-hover-brown" href="<?php echo base_url('index.php/main/tentang')?>">
                <i class="fa fa-comments-o"></i> Tentang</a>
            <a class="w3-bar-item w3-button w3-hover-brown" href="<?php echo base_url('index.php/main/bantuan')?>">
                <i class="fa fa-question-circle-o"></i> Bantuan</a>
            <a class="w3-bar-item w3-button w3-hover-brown w3-margin-top" href="<?php echo base_url('index.php/login/logout')?>">Keluar</a>
        </div>
    </nav>

    <!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>