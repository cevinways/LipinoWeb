<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:300px; margin-right: 150px">
    <div class="w3-row w3-padding-64">
        <div class="w3-text-brown">
            <h2>Tentang</h2>
            <h3>Apa Itu Lipino?</h3>
            <p align='justify'>
                LIPINO (Layanan Identitas Penduduk Online Indonesia) adalah layanan yang mempermudah dalam pembuatan ktp. LIPINO merupakan
                website yang dibuat dengan tujuan untuk mempermudah penduduk Indonesia dalam pembuatan KTP secara online.
                Adanya website LIPINO penduduk Indonesia dapat lebih menghemat biaya dan waktu, karena layanan pembuatan
                KTP ini secara online dan juga penduduk Indonesia yang tidak mempunyai KTP tidak harus pergi ke kantor terpadu
                pada masing-masing daerahnya. Layanan online ini tidak menguras tenaga karena pembuatanya dilakukan secara
                online. Layanan ini diperuntukan bagi penduduk Indonesia yang belum mempunyai KTP elektronik maupun yang
                akan merubah KTP biasa menjadi KTP elektronik. Layanan ini dilakukan karena pada kantor terpadu di masing-masing
                daerah terjadi antrean yang panjang hingga proses pendaftaran dilakukan hingga memakan waktu berjam-jam.
                Oleh karena itu terbentuklah LIPINO layanan yang memberikan fasilitas pembuatan KTP secara online tanpa memakan
                banyak waktu hingga berjam – jam.
            </p>
            <br />
            <h3>Apa Itu KTP?</h3>
            <p align='justify'>
                KTP (Kartu Tanda)merupakan identitas penduduk indonesia yang merupakan tanda pengenal paling umum di indonesia. Namu, tidak
                sedikit orang indonesia yang masih belum memiliki KTP, dikarenakan kurang pahamnya akan kegunaan KTP itu
                untuk apa, kurang nya pendekatan pemerintah, terdapat beberapa permasalahan yang dihadapi khususnya pada
                perekaman e-KTP serta keluhan masyarakat mengenai pelayanan pembuatan e-KTP, masih adanya orang yang beranggapan
                bahwa kalau membuat KTP itu tidak mudah dengan banyaknya persyaratan, dan bhkan kurangnya waktu luang untuk
                membuat KTP tersebut. Pada saat ini masyrakat indonesia sudah di mudahkan dalam pembuatan e-KTP dengan hanya
                membawa KK (Kartu Keluarga) ke Kantor Capil sudah bisa membuat e-KTP dengan bantuan pegawai. namun, hal itu
                juga belum bisa dipastikan kalau masyrakat indonesia sudah mempunyai KTP karena mungkin keurang pahamnya
                akan teknologi bagi orang tua yang berusia lanjut dan atau bahkan orang berfikir bahwa KTP yang lama masih
                terpakai jadi tidak perlu membuat KTP. Oleh karena itu, pelaksaan LIPINO (Layanan Identitas Penduduk Indonesia
                Online) ini bisa membantu dan dapat meningkatkan kesadaran dari orang-orang yang beranggapan bahwa kalu membuat
                KTP itu tidak mudah dan banyak syaratnya, Kelompok ini ingin menerapkan sistem pendaftaran KTP yang berbasis
                teknologi.
            </p>
            <br />
        </div>
    </div>
</div>