<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:300px; margin-right: 150px">
    <div class="w3-row w3-padding-64">
        <div class="w3-text-brown">
            <h2>Bantuan</h2>
            <h3>Bagaimana cara membuat akun?</h3>
            <ol>
                <li>Masuk ke website LIPINO yaitu lipino.ml</li>
                <li>Masukkan NIK, Email, dan Password</li>
                <li>Simpan data anda</li>
            </ol>
            <br />
            <h3>Bagaimana cara mendaftar KTP di LIPINO?</h3>
            <ol>
                <li>Buat akun pada website LIPINO</li>
                <li>Apabila sudah memiliki akun, bisa langsung melakukan login dengan memasukkan NIK dan Password</li>
                <li>Setelah melakukan login atau masuk dalam beranda, anda diharapkan untuk mengisi data diri anda dengan lengkap
                    dengan benar</li>
                <li>Jika sudah melakukan pengisian data, lakukan upload foto formal</li>
                <li>Simpan data yang sudah anda isikan agar admin bisa memverifikasi akun anda</li>
                <li>Tunggu konfirmasi dari admin melalui email maupun SMS</li>
            </ol>
            <br />
            <h3>Bagaimana cara memperbarui KTP di LIPINO?</h3>
            <ol>
                <li>Buat akun pada website LIPINO</li>
                <li>Apabila sudah memiliki akun, bisa langsung melakukan login dengan memasukkan NIK dan Password</li>
                <li>Setelah melakukan login atau masuk dalam beranda, anda diharapkan untuk mengisi data diri anda dengan lengkap
                    dengan benar</li>
                <li>Jika sudah melakukan pengisian data, lakukan upload foto formal</li>
                <li>Unggah scan KTP lama anda untuk bukti data</li>
                <li>Simpan data yang sudah anda isikan agar admin bisa memverifikasi akun anda</li>
                <li>Tunggu konfirmasi dari admin melalui email maupun SMS</li>
            </ol>
            <br />
        </div>
    </div>
</div>